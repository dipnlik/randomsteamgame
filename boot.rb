require 'nokogiri'
require 'open-uri'
require 'sinatra'
require 'slim'

CACHE_TIME_LIMIT = 7 * 24 * 60 * 60 # one week
CACHE_DIR = 'tmp/cache'

get '/' do
  slim :index
end

post '/rsg' do
  redirect to("/id/#{params[:steam_id]}")
end

get '/id/:steam_id' do
  xml_game = random_game_for_user(params[:steam_id].downcase)
  @game = game_hash_from_xml_game(xml_game)
  slim :show
end

def random_game_for_user(user_id)
  filename = "#{CACHE_DIR}/#{user_id}.xml"
  
  if File.exists?(filename) && (Time.now - File.mtime(filename) < CACHE_TIME_LIMIT)
    doc = Nokogiri::XML open(filename)
  else
    doc = Nokogiri::XML(open("http://steamcommunity.com/id/#{user_id}/games?tab=all&xml=1"))
    File.open(filename, 'w') {|f| f << doc.to_xml }
  end
  
  doc.css("game").to_a.sample
end

def game_hash_from_xml_game(xml_game)
  kv_array = xml_game.children.map do |node|
    [node.name.to_sym, node.content] unless node.name == 'text'
  end.compact.flatten
  
  Hash[*kv_array]
end

helpers do
  def playtime(hours_last_2_weeks, hours_on_record)
    strings = []
    strings << "#{hours_last_2_weeks} hrs last two weeks" if hours_last_2_weeks
    strings << "#{hours_on_record} hrs on record" if hours_on_record
    strings.join(' / ')
  end
end
